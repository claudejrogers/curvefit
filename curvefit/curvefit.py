# -*- coding: utf-8 -*-
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from .model_functions import (
    two_parameter, two_parameter_pd,
    three_parameter, three_parameter_pd,
    four_parameter, four_parameter_pd,
    five_parameter, five_parameter_pd,
    get_symbolic_function, eq, dvardx
)


# I have numpy 2.0.0 on my machines. Comment this out if using earlier numpy
np.seterr(all='ignore')


class CurveFit(object):
    """
    Fit data to a model function

    Parameters
    ----------

    xdata : ndarray (one dimension)
        x data

    ydata : ndarray (one dimension)
        y data

    model : string
        Model can be a named model or an equation.
        Named model options:
            'two_parameter'
            'three_parameter'
            'four_parameter'
            'five_parameter'
        Equations must have an x variable and each variable should be of the
        form `var0, var1, var2, ..., varn`. For example, the Michaelis-Menten
        equation would have the form:
            '(var0 * x) / (var1 + x)'
        Equations should be formated such that they would run in python by calling
            eval(model)
        assuming x, var0, var1, ..., varn were defined and numpy was imported as np.

    var : list or ndarray (one dimension)
        Initial guess for var0--n.
        For the named models (hill equations), var = [ic50, hill, max, min, symmetry]
    """
    MAX_ITER = 500
    THRESH = 1.0e-15

    def __init__(self, xdata, ydata, model, var):
        models = {
            'two_parameter': [two_parameter, two_parameter_pd],
            'three_parameter': [three_parameter, three_parameter_pd],
            'four_parameter': [four_parameter, four_parameter_pd],
            'five_parameter': [five_parameter, five_parameter_pd]
        }
        self.msg = ''
        self.param = var
        self.var = var
        self.nvar = len(var)
        self.model = model
        self.k = 0
        self.x = np.array(xdata)
        self.y = np.array(ydata)
        self.m = []
        self.d = []
        if model in models:
            self.eqn, funcarray = models.get(model)
            self.funcarray = funcarray()
        else:
            self.model = get_symbolic_function(model)
            self.eqn = eq(self.model, self.nvar)
            self.funcarray = dvardx(self.model, self.nvar)

    def equation(self):
        self.m = self.eqn(self.x, *self.var)

    def derivatives(self):
        self.d = [df(self.x, *self.var) for df in self.funcarray]

    def get_f(self):
        self.equation()
        return self.y - self.m

    def get_jac(self):
        args = []
        self.derivatives()
        for i in self.d:
            if isinstance(i, int):
                i = self.x ** 0.0
            args.append(i)
        jt = np.array(args)
        return -1 * jt.T

    def levenberg_marquardt(self):
        """
        Curvefit routine
        """
        N = len(self.var)
        self.k = 0
        v = 2
        J = self.get_jac()
        f = self.get_f()
        a = np.dot(J.T, J)
        g = np.dot(J.T, f)
        mu = 1.0e-3 * max(np.diag(a))
        while np.linalg.norm(g, np.inf) >= self.THRESH and self.k < self.MAX_ITER:
            self.k += 1
            muI = mu * np.eye(N)
            h = np.linalg.solve((a + muI), -g)
            self.param = [i for i in self.var]
            self.var += h
            if np.linalg.norm(self.var - self.param) <= 1.0e-20:
                return (self.k, self.var)
            f_new = self.get_f()
            dF1 = np.dot(f.T, f)
            dF2 = np.dot(f_new.T, f_new)

            dF = 0.5 * (dF1 - dF2)
            dL = 0.5 * np.dot(h.T, ((mu * h) - g))
            d = dF / dL
            if d > 0:
                J = self.get_jac()
                f = f_new
                a = np.dot(J.T, J)
                g = np.dot(J.T, f)
                mu *= max(1.0 / 3.0, (1 - (2 * d - 1) ** 3))
                v = 2
            else:
                self.var = self.param[:]
                mu *= v
                v *= 2
        return (self.k, self.var)

    def plot(self, plotname):
        """
        Plot curvefit results.

        Parameters
        ----------

        plotname : path
            Path to save plot
        """
        plt.rcParams['xtick.labelsize'] = 12
        plt.rcParams['ytick.labelsize'] = 12
        plt.figure(figsize=(9, 7))
        ax = plt.subplot(111)
        ax.set_xscale('log')
        ax.grid(which='both', axis='x')
        ax.grid(axis='y')
        x = self.x
        y = self.y
        x0 = np.linspace(x.min(), x.max(), num=100000)
        m = self.eqn(x0, *self.var)
        ax.plot(x, y, 'ks--')
        ax.plot(x0, m, 'k')

        # adjust plot box size to accomidate label
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.25,
                         box.width, box.height * 0.8])

        ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.2), ncol=2)
        plt.savefig(plotname, dpi=80)
