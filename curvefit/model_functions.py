import re
import numpy as np
import sympy as s


def find_nvars(function):
    return len(set(re.findall('var[0-9]+', function)))


def get_symbolic_function(function):
    return s.sympify(function)


def eq(symfunc, nvars):
    symbols = s.symbols("x,var:{0}".format(nvars))
    return s.lambdify(symbols, symfunc, "numpy")


def dvardx(symfunc, nvars):
    symbols = s.symbols("x,var:{0}".format(nvars))
    derivatives = [s.diff(symfunc, v) for v in symbols[1:]]
    return [s.lambdify(symbols, func, "numpy") for func in derivatives]


# Equations for the models
def two_parameter(x, var0, var1):
    return 100 / ((var0 / x) ** var1 + 1)


def three_parameter(x, var0, var1, var2):
    return var2 / ((var0 / x) ** var1 + 1)


def four_parameter(x, var0, var1, var2, var3):
    return var3 + (((var2 - var3) / (1 + (var0 / x) ** var1)))


def five_parameter(x, var0, var1, var2, var3, var4):
    return (
        ((var2 - var3) /
         (1 + (x / (var0 * 10 ** (
             (1.0 / var1) * np.log((2.0 / var4) - 1)
         ))) ** (-var1)) ** var4) + var3
    )


# Equations for the partial derivatives of the models
def two_parameter_pd():
    d0 = lambda x, *var: (
        -100 * var[1] * (var[0] / x) ** var[1] /
        (var[0] * ((var[0] / x) ** var[1] + 1) ** 2)
    )
    d1 = lambda x, *var: (
        -100 * (var[0] / x) ** var[1] * np.log(var[0] / x) /
        ((var[0] / x) ** var[1] + 1) ** 2
    )
    return [d0, d1]


def three_parameter_pd():
    d0 = lambda x, *var: (
        -var[1] * var[2] * (var[0] / x) ** var[1] /
        (var[0] * ((var[0] / x) ** var[1] + 1) ** 2)
    )
    d1 = lambda x, *var: (
        -var[2] * (var[0] / x) ** var[1] *
        np.log(var[0] / x) / ((var[0] / x) ** var[1] + 1) ** 2
    )
    d2 = lambda x, *var: (
        1 / ((var[0] / x) ** var[1] + 1)
    )
    return [d0, d1, d2]


def four_parameter_pd():
    d0 = lambda x, *var: (
        -var[1] * (var[0] / x) ** var[1] * (var[2] - var[3]) /
        (var[0] * ((var[0] / x) ** var[1] + 1) ** 2)
    )
    d1 = lambda x, *var: (
        -(var[0] / x) ** var[1] * (var[2] - var[3]) * np.log(var[0] / x) /
        ((var[0] / x) ** var[1] + 1) ** 2
    )
    d2 = lambda x, *var: 1 / ((var[0] / x) ** var[1] + 1)
    d3 = lambda x, *var: 1 - 1 / ((var[0] / x) ** var[1] + 1)
    return [d0, d1, d2, d3]


def five_parameter_pd():
    d0 = lambda x, *var: (
        -var[1] * var[4] * (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1]) *
        (1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])) ** (-var[4]) *
        (var[2] - var[3]) /
        (var[0] * (1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])))
    )
    d1 = lambda x, *var: (
        -var[4] * (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1]) *
        (1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])) ** (-var[4]) *
        (var[2] - var[3]) * (-np.log(10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) - 1.0 * np.log(10) *
            np.log(-1 + 2.0 / var[4]) / var[1]) /
        (1 + (10 ** (-1.0 * np.log(-1 + 2.0 / var[4]) /
                     var[1]) * x / var[0]) ** (-var[1]))
    )
    d2 = lambda x, *var: (
        (1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])) ** (-var[4])
    )
    d3 = lambda x, *var: (
        1 - (1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])) ** (-var[4])
    )
    d4 = lambda x, *var: (
        (1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])) ** (-var[4]) *
        (var[2] - var[3]) * (-np.log(1 + (10 ** (
            -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
        ) * x / var[0]) ** (-var[1])) + 2.0 *
            (10 ** (-1.0 * np.log(-1 + 2.0 / var[4]) / var[1]) *
             x / var[0]) ** (-var[1]) * np.log(10) /
            (var[4] * (-1 + 2.0 / var[4]) * (1 + (10 ** (
                -1.0 * np.log(-1 + 2.0 / var[4]) / var[1]
            ) * x / var[0]) ** (-var[1]))))
    )
    return [d0, d1, d2, d3, d4]
