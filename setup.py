from distutils.core import setup


setup(
    name='Curvefit',
    version='0.1',
    description='Implementation of Levenberg-Marquardt algorithm',
    author='Claude Rogers',
    author_email='cjrogers@caltech.edu',
    url='https://bitbucket.org/claudejrogers/curvefit',
    packages=['curvefit']
)
